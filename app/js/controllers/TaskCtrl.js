ucionicaApp.controller('TaskCtrl', ['$scope', '$location', 'tasksData', function($scope, $location, tasksData){
	/*
	$scope.addTask = function(){

		$http.post('/newtask', {
			description: $scope.taskDesc,
			duration: $scope.taskDur
		})
		.then(function onSuccess(response){
			window.location = '/opentasks';
		})
		.catch(function onError(err){
			console.log('Err: ' + err);
		});
	}*/

	$scope.addTask = function(task){
		tasksData.save(task)
			.$promise
			.then(function(response){
				$location.path('/opentasks');
			})
			.catch(function(err){
				console.log(err);
			})
	}

	$scope.goToTasks = function(){
		$location.path('/opentasks');
	}


}]);