ucionicaApp.controller('OpenTasksCtrl', ['$scope', '$route', '$interval', 'tasksData', function($scope, $route, $interval, tasksData){

	$scope.getTasks = function(){
		tasksData.getAllTasks()
			.$promise
			.then(function(allTasks){
				$scope.tasks = allTasks;			
			})
			.catch(function(err){
				console.log(err);
			})
	}


	$scope.tasks = $route.current.locals.tasks;

  	$interval($scope.getTasks,2000);


  	var tick = function() {
    	$scope.clock = Date.now();
  	}
  	tick();
  	$interval(tick, 1000);





}]);