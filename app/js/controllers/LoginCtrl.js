ucionicaApp.controller('LoginCtrl', ['$scope', '$location', '$cookies', '$timeout', 'userData', 'adminData', function($scope, $location, $cookies, $timeout, userData, adminData){

	$scope.runLogin = function(){
		userData.userLogin($scope.logindata)
			.$promise
			.then(function(response){
				$cookies.put('role', 'student');
				$scope.$parent.getRole();
				$location.path('/me');
			})
			.catch(function(err){
				console.log(err);
			})		
	}

	$scope.runAdminLogin = function(){
		adminData.adminLogin($scope.admindata)
			.$promise
			.then(function(response){
				$cookies.put('role', 'admin');
				$scope.$parent.getRole();
				$location.path('/admin');
			})
			.catch(function(err){
				$scope.invalidCredentials = true;
				$timeout(function(){
					$scope.invalidCredentials = false;
				}, 2000);
				$scope.admindata = {};
			})		
	}	

	$scope.goToAdminLogin = function(){
		$location.path('/loginadmin');
	}

	$scope.goToStudentLogin = function(){
		$location.path('/');
	}		
}]);