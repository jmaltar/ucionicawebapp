ucionicaApp.controller('StudentCtrl', ['$scope', '$route', '$interval', 'tasksData','userData', function($scope, $route, $interval, tasksData, userData){

	$scope.getAllUsers = function(){
		userData.getAllUsers()
			.$promise
			.then(function(allUsers){
				$scope.users = allUsers;				
			})
			.catch(function(err){
				console.log(err);
			})
	}
	
	$scope.users = $route.current.locals.users;

	$interval($scope.getAllUsers,2000);
	
	$scope.getTasks = function(){
		tasksData.getAllTasks()
			.$promise
			.then(function(allTasks){
				$scope.tasks = allTasks;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	$scope.tasks = $route.current.locals.tasks;

	$interval($scope.getTasks, 2000);

	/*
  	$interval($scope.getTasks,2000);
  	$interval($scope.getAllUsers,2000);	*/



}]);