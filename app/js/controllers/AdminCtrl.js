ucionicaApp.controller('AdminCtrl', ['$scope', '$route', '$timeout', '$cookies','$location','adminData', 'userData', 'tasksData', 'Logout', function($scope, $route, $timeout, $cookies, $location,adminData, userData, tasksData, Logout){
	

	$scope.getAdmin = function(){
		userData.getUser('/getadmin')
			.$promise
			.then(function(user){
				$scope.user = user;
			})
			.catch(function onError(err){
				console.log(err);
			})			
	}


	$scope.user = $route.current.locals.admin;


	$scope.deleteTasks = function(){
		tasksData.deleteTasks()
			.$promise
			.then(function onSuccess(allTasks){
				$scope.tasksDeleted = true;
				$timeout(function(){
					$scope.tasksDeleted = false;
				}, 2000);
			})
			.catch(function onError(err){
				console.log(err);
			})				
	}


	$scope.deleteStudents = function(){
		userData.deleteStudents()
			.$promise
			.then(function onSuccess(allStudents){
				$scope.studentsDeleted = true;
				$timeout(function(){
					$scope.studentsDeleted = false;
				}, 2000);
			})
			.catch(function onError(err){
				console.log(err);
			})				
	}

	$scope.changeAdminName = function(){
		adminData.changeName($scope.new)
			.$promise
			.then(function(response){
				$scope.new = {};
				$scope.nameChanged = true;
				$timeout(function(){
					$scope.nameChanged = false;
				}, 2000);				
				$scope.getAdmin();
			})
			.catch(function(err){
				console.log(err);
			})		
	}

	$scope.changeAdminEmail = function(){
		adminData.changeEmail($scope.new)
			.$promise
			.then(function(response){
				$scope.new = {};
				$scope.emailChanged = true;
				$timeout(function(){
					$scope.emailChanged = false;
				}, 2000);				
				$scope.getAdmin();
			})
			.catch(function(err){
				console.log(err);
			})		
	}

	$scope.changeAdminPassword = function(){
		adminData.changePassword($scope.new)
			.$promise
			.then(function(response){
				$scope.new = {};
				$scope.passwordChanged = true;
				$timeout(function(){
					$scope.passwordChanged = false;
				}, 2000);				
				$scope.getAdmin();
			})
			.catch(function(err){
				console.log(err);
			})		
	}

	$scope.adminLogout = function(){
		Logout.adminlogout()
			.$promise
			.then(function(response){
				$cookies.remove('role');
				$scope.$parent.getRole();
				$location.path('/loginadmin');
			})
			.catch(function(err){
				console.log(err);
			})


	}	

}]);