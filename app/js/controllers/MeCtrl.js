ucionicaApp.controller('MeCtrl', ['$scope', '$route', '$interval', '$cookies', '$location','tasksData', 'userData', 'Logout', function($scope, $route, $interval, $cookies, $location, tasksData, userData, Logout){
	
	$scope.getUser = function(){
		userData.getUser('/getuser')
			.$promise
			.then(function(user){
				$scope.user = user;			
			})
			.catch(function onError(err){
				console.log(err);
			})			
	}

	$scope.user = $route.current.locals.user;


	$interval($scope.getUser,2000);	


	$scope.getTasks = function(){
		tasksData.getAllTasks()
			.$promise
			.then(function(allTasks){
				$scope.tasks = allTasks;
			})
			.catch(function(err){
				console.log(err);
			})
	}

	$scope.tasks = $route.current.locals.tasks;
  	
  	$interval($scope.getTasks,2000);

  	
  	var tick = function() {
    	$scope.clock = Date.now();
  	}

  	tick();
  	$interval(tick, 1000);
  		


	$scope.solved = function(id){
		var str1 = 'solvedtask' + id;
		var str2 = 'helptask' + id;
		$scope.user[str1] = true;
		$scope.user[str2] = false;
		$scope.updateUser(str1, str2, true, false);
	}

	$scope.inProgress = function(id){
		var str1 = 'solvedtask' + id;
		var str2 = 'helptask' + id;		
		$scope.user[str1] = false;		
		$scope.user[str2] = false;
		$scope.updateUser(str1, str2, false, false);
	}

	$scope.needHelp = function(id){
		var str1 = 'solvedtask' + id;
		var str2 = 'helptask' + id;		
		$scope.user[str1] = false;			
		$scope.user[str2] = true;
		$scope.updateUser(str1, str2, false, true);	
	}

	$scope.updateUser = function(str1, str2, bool1, bool2){
		var toUpdate = {"userid": $scope.user.id,
						"str1": str1, 
						"str2": str2, 
						"bool1":bool1, 
						"bool2":bool2
					};
		
		userData.updateUser(toUpdate)
			.$promise
			.then(function(response){
			})
			.catch(function(err){
				console.log()
			})
	}

	$scope.logout = function(){
		Logout.logout()
			.$promise
			.then(function(response){
				$cookies.remove('role');
				$scope.$parent.getRole();
				$location.path('/');
			})
			.catch(function(err){
				console.log(err);
			})


	}				

}]);