'use strict';

ucionicaApp.filter('solvedTasks', function(){
	return function(task) {
		switch(task) {
			case false: return 'img/ok.png';
			case true: return 'img/ok_active.png';
		}
	}
});

ucionicaApp.filter('inProgress', function(){
	return function(task) {
		switch(task) {
			case false: return 'img/in_progress.png';
			case true: return 'img/in_progress_active.png';
		}
	}
});

ucionicaApp.filter('needHelp', function(){
	return function(task) {
		switch(task) {
			case false: return 'img/help.png';
			case true: return 'img/help_active.png';
		}
	}
});

ucionicaApp.filter('set0', function(){
	return function(duration) {
		if(duration <= 0) {return 0;}
		else {return duration;}
	}
});