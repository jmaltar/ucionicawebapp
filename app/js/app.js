'use strict';

var ucionicaApp = angular.module('ucionicaApp', ['ngResource','ngRoute', 'ngCookies'])
	.config(function($routeProvider, $locationProvider){
		$routeProvider.when('/',
			{
				templateUrl: 'templates/login.html',
				controller: 'LoginCtrl'				
			});
		$routeProvider.when('/loginadmin', 
			{
				templateUrl: 'templates/loginadmin.html',
				controller: 'LoginCtrl',				
			});		
		$routeProvider.when('/admin', 
			{
				templateUrl: 'templates/admin.html',
				controller: 'AdminCtrl',
				resolve: {				
					admin: function($route, userData){
						return userData.getUser('/getadmin').$promise;
					}
				}
			});
		$routeProvider.when('/me', 
			{
				templateUrl: 'templates/me.html',
				controller: 'MeCtrl',
				resolve: {
					user: function($route, userData){
						return userData.getUser('/getuser').$promise;
					},
					tasks: function($route, tasksData){
						return tasksData.getAllTasks().$promise;
					}
				}
			});
		$routeProvider.when('/student', 
			{
				templateUrl: 'templates/student.html',
				controller: 'StudentCtrl',
				resolve: {
					users: function($route, userData){
						return userData.getAllUsers().$promise;
					},
					tasks: function($route, tasksData){
						return tasksData.getAllTasks().$promise;
					}
				}
			});		
		$routeProvider.when('/newtask', 
			{
				templateUrl: 'templates/newtask.html',
				controller: 'TaskCtrl',
			});
		$routeProvider.when('/opentasks', 
			{
				templateUrl: 'templates/opentasks.html',
				controller: 'OpenTasksCtrl',
				resolve: {				
					tasks: function($route, tasksData){
						return tasksData.getAllTasks().$promise;
					}
				}				

			});
		$routeProvider.when('/about',
			{
				template: 'Version: 0.1'
			});		
		$routeProvider.otherwise({redirectTo: '/'});
		$locationProvider.html5Mode(true);			
		})


	.run(function($rootScope, $location, $cookies) {
	    $rootScope.$on("$routeChangeStart", function(next, current) {
			if(!$cookies.get('role')){
				if(current.$$route.templateUrl == "templates/loginadmin.html"){
					$location.path('/loginadmin');
				} else {
					$location.path('/');
				}
			}

			else if($cookies.get('role') == "admin"){
				if(current.$$route.templateUrl == "templates/loginadmin.html" || current.$$route.templateUrl == "templates/login.html"){
					$location.path('/admin');
				}

				if(current.$$route.templateUrl == "templates/me.html") {
					$location.path('/admin');
				} 

			}

			else{
				if(current.$$route.templateUrl == "templates/login.html" || current.$$route.templateUrl == "templates/loginadmin.html"){
					$location.path('/me');
				}

				if(current.$$route.templateUrl == "templates/newtask.html") {
					$location.path('/me');
				} 

				if(current.$$route.templateUrl == "templates/admin.html") {
					$location.path('/me')
				}

			}	



	    });
});
