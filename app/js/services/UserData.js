ucionicaApp.service('userData', function($resource){

	return {

		updateUser: function(user){
			var resource = $resource('/data/User');			
			return resource.save(user);
		},

		getUser: function(getLocation){
			var resource = $resource(getLocation);
			return resource.get();
		},

		deleteStudents: function(){
			var resource = $resource('/deletestudents');
			return resource.save();
		},

		userLogin: function(user){
			var resource = $resource('/login');
			return resource.save(user);
		},

		getAllUsers: function(){
			var resource = $resource('/getusers');			
			return resource.query();
		},
	};
});