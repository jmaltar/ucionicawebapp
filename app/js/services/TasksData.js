ucionicaApp.service('tasksData', function($resource){

	return {

		getAllTasks: function(){
			var resource = $resource('/data/Task');			
			return resource.query();
		},
		save: function(task){
			var resource = $resource('/data/Task');
			return resource.save(task);
		},

		deleteTasks: function(){
			var resource = $resource('/deletetasks');
			return resource.save();
		}
	};
});