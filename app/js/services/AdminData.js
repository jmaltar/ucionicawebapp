ucionicaApp.service('adminData', function($resource){

	return {

		adminLogin: function(admin){
			var resource = $resource('/adminlogin');			
			return resource.save(admin);
		},

		changeName: function(newName){
			var resource = $resource('/changename');			
			return resource.save(newName);
		},

		changeEmail: function(newEmail){
			var resource = $resource('/changeemail');			
			return resource.save(newEmail);
		},

		changePassword: function(newPassword){
			var resource = $resource('/changepassword');			
			return resource.save(newPassword);
		}						


	};
});