ucionicaApp.service('Logout', function($resource){

	return {
		logout: function(){
			var resource = $resource('/logout');			
			return resource.get();
		},	
		adminlogout: function(){
			var resource = $resource('/adminlogout');			
			return resource.get();
		},						
	};
});