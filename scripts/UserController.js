var db = require('diskdb');
db = db.connect('app/data/', ['User', 'Task']);
var gravatar = require('gravatar');

module.exports = {

	adminLogin: function(req, res){
		var maybeAdmin = req.body;
    	var admin = db.User.findOne({role: "admin"});	    	    	

		if(admin.password != maybeAdmin.passw || admin.name != maybeAdmin.name){
			return res.sendStatus(401);
		} else {
			maybeAdmin = admin;
			req.session.me = maybeAdmin.id;
			return res.send();
		}
	},

	adminLogout: function(req, res){
		req.session.me = null;
		return res.send();			
	},

	getAdmin: function(req, res){
		user = db.User.findOne({id: req.session.me});
		res.send(user);
	},	

	changeName: function(req, res){
		db.User.update({role:"admin"}, {name: req.body.newName});
		return res.send();
	},

	changeEmail: function(req, res){
		newGravatarUrl = gravatar.url(req.body.newEmail);
		db.User.update({role:"admin"}, {email: req.body.newEmail, gravatarUrl: newGravatarUrl});
		return res.send();
	},

	changePassword: function(req, res){
		db.User.update({role:"admin"}, {password: req.body.newPassword});
		return res.send();
	},

	userLogin: function(req, res){
		var newUser = req.body;
		newUser["id"] = db.User.find().length+1;
		newUser["role"] = "student";
		newUser["gravatarUrl"] = gravatar.url(newUser.email);

		var tasks = db.Task.find();

		for(task of tasks){
			solvedtask  = "solvedtask" + String(task.taskid);
			helptask  = "helptask" + String(task.taskid);
			newUser[solvedtask] = false;
			newUser[helptask] = false;
		}

		db.User.save(newUser);
		req.session.me = newUser.id;
		return res.send();

	},

	userLogout: function(req, res){
		db.User.remove({id: req.session.me});
		req.session.me = null;
		return res.send();			
	},

	getUser: function(req, res){
		user = db.User.findOne({id: req.session.me});
		res.send(user);
	},

	updateUser: function(req, res){
		var data = req.body;
		var solvedtask  = data.str1;
		var helptask  = data.str2;
		var attrjson = { };
		attrjson[solvedtask] = data.bool1;
		attrjson[helptask] = data.bool2;		
		db.User.update({id: data.userid}, attrjson);
		return res.send();

	},

	getUsers: function(req, res) {
		res.send(db.User.find({"role": "student"}));
		res.end();
	},

	deleteStudents: function(req, res) {
		db.User.remove({role: "student"});
		res.send();
	}
			



};