var express = require('express');
var path = require('path');
var app = express();
var session = require('express-session');
app.use(session({secret: 'keyboard cat',resave: true,saveUninitialized: true}));
var rootPath = path.normalize(__dirname + '/../');
var bodyParser = require('body-parser');

var taskc = require('./TaskController');
var userc = require('./UserController');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static( rootPath + '/app'));



//TaskController
app.post('/data/Task', taskc.setTask);
app.get('/data/Task', taskc.getTasks);
app.post('/deletetasks', taskc.deleteTasks);

//UserController
app.post('/adminlogin', userc.adminLogin);
app.get('/adminlogout', userc.adminLogout);
app.get('/getadmin', userc.getAdmin);
app.post('/changename', userc.changeName);
app.post('/changeemail', userc.changeEmail);
app.post('/changepassword', userc.changePassword);
app.post('/login', userc.userLogin);
app.get('/logout', userc.userLogout);
app.get('/getuser', userc.getUser);
app.post('/data/User', userc.updateUser);
app.get('/getusers', userc.getUsers);
app.post('/deletestudents', userc.deleteStudents);

app.get('*', function(req, res) {res.sendFile(rootPath+'/app/index.html');});

app.listen(8080);
console.log('Listening on port ' + 8080 + '...');