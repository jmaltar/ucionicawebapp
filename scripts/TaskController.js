var db = require('diskdb');
db = db.connect('app/data/', ['User', 'Task']);

module.exports = {

	setTask: function(req, res){
		var taskid = db.Task.find().length + 1;
		var task = {};
		task["description"] = req.body.description;
		var t = new Date();
		t.setSeconds(t.getSeconds() + req.body.duration*60);
		task["duration"] = t.getTime();
		task["taskid"] = taskid;
		db.Task.save(task);

		var solvedtask  = "solvedtask" + String(taskid);
		var helptask  = "helptask" + String(taskid);		
		var attrjson = { };
		attrjson[solvedtask] = false;
		attrjson[helptask] = false;

		db.User.update({role: "student"},attrjson,{multi:true,upsert:false});
		
		return res.send();
	},

	getTasks: function(req, res) {
		res.send(db.Task.find())
		res.end();
	},

	deleteTasks: function(req, res) {
		var students = db.User.find({role: "student"});
		var length = students.length;
		db.User.remove({role: "student"});
		for(var i = 0; i < length; i++){
			student = students[i];
			db.User.save({
				name: student["name"], 
				id: student["id"],
				email: student["email"],
				number: student["number"],
				role: "student",
				gravatarUrl: student["gravatarUrl"]
			})
		}
		db.Task.remove();
		db = db.connect('app/data/', ['Task']);		
		res.send();
	}	

};

