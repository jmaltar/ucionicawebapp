# Ucionica.webapp — Simple Angular JS application for classroom management

### Server

A node server is provided. 
IMPORTANT: From the command line run "npm install" and then "server.bat" 

### Running the application

Navigate your browser to `http://localhost:8080/` to see the app running in your browser.
Default admin credentials are "admin" and "admin"

